# An Ubuntu base provides for a rather thin kickstart
FROM ubuntu:20.10

# Base Docker image preparations to host Helios
RUN set -e \
    \
# Make sure necessary tools are accessible
    && apt-get -y update \
    && apt-get -y install patch python2 postgresql-client gettext \
    && ln -s /usr/bin/python2 /usr/bin/python \
    \
# Helios will be run by 'helios' user
    && useradd -m -k /etc/skel/ -s /bin/bash helios

# Everything Helios related will occur inside helios user's home
WORKDIR /home/helios/

# Copy context
COPY . .

# Helios setup
RUN set -e \
    \
# Wrap up Python installation by installing 'pip'
    && python2 get-pip.py \
    \
# Enforce restrictive access modes to critical files
    && chmod 0600 .config .pgpass \
    \
# Install Helios code root and patch it
    && tar xvzf helios-server.tar.gz \
    && patch -d helios-server -p1 < helios.patch \
    \
# Configure Helios
    && chmod +x configure.sh \
    && ./configure.sh \
    \
# Install Helios requirements
    && pip install -r helios-server/requirements.txt \
    \
# Set up Helios translations
    && django-admin compilemessages \
    && mv locale ./helios-server/ \
    \
# Set up main container executable
    && chmod +x helios.sh \
    && mv helios.sh /usr/bin/ \
    \
# Enforce access restrictions to Helios code root and configuration
    && chown -R helios:helios /home/helios/ \
    && chmod 0700 /home/helios \
    \
# Clean up
    && rm configure.sh get-pip.py helios-server.tar.gz helios.patch

# Expose the port on which Helios listens
EXPOSE $HELIOS_PORT

# Finally, switch to Helios' root and user and set container entry point
WORKDIR /home/helios/helios-server/
USER helios
ENTRYPOINT ["helios.sh"]
