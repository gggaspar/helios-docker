#!/bin/sh

####
## USE THIS TO BUILD/START THE NECESSARY DOCKER STACK FOR HELIOS
###

# Source and destination file for variables
src='.config'
dst='.env'

# Send a configuration variable to a destination file
# Expects:
#  $1: the configuration variable to be searched for
#
# Listens:
#  src: name of the source file from which variables are fetched
#  dst: name of destination file to which variables are sent
send_variable() {
    grep "$1=" "$src" | sed 's/ *#.*//' >> "$dst"
}

# Docker allows for a great deal of configurability by reading variable
# values from a '.env' file and using those inside its scripts. Here, a
# '.env' file is created containing only what Docker needs to build the
# image stack.
echo -n > ".env"

# Make sure '.env' file now exists and is writable by the script
if [ ! -w ".env" ]; then
    echo "error: '.env' file does not exist or no write permission is granted"
    exit 1
fi

# Send necessary configuration variables to '.env' so Docker may access them
send_variable 'HELIOS_PORT'
send_variable 'HOST_PORT'
send_variable 'DB_PASSWORD'
send_variable 'DB_USER'
send_variable 'DB_HOST'
send_variable 'BROKER_HOST'
send_variable 'BROKER_PORT'

# Finally, run Helios (will build everything from scratch first time)
docker-compose up --build
