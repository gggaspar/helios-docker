#!/bin/sh

####
## FOR USE BY DOCKER
###

# Fetch configuration variables
. ./.config


# Expected file locations
db_pgpass="./.pgpass"
helios_settings="$HELIOS_PATH/settings.py"
helios_reset="$HELIOS_PATH/reset.sh"


# Configures a variable in a specific file. All occurrences of the variable to
# be configured are expected to be within single quotes
# Expects:
#  $1: the (strong-quoted) variable to configure
#  $2: the file in which to configure the variable
configure_quotes () {
    value=$(eval "echo $1")
    # using vertical bar to avoid clashes (timezone values use '/')
    sed -i "s|'$1'|'$value'|g" "$2"
}

# Configures a variable in a specific file. Occurrences are searched for without
# any special surrounding formating.
# THIS CAN CAUSE CONFIGURATION CLASHES IF VARIABLE NAMES HAVE COMMON RADICALS.
# USE WITH CARE.
# Expects:
#  c.f. configure_quotes
configure_raw () {
    value=$(eval "echo $1")
    # using vertical bar to avoid clashes (timezone values use '/')
    sed -i "s|$1|$value|g" "$2"
}



# Make sure Helios' root (helios-server) is present
if [ ! -d "$HELIOS_PATH" ]; then
    echo "can't find Helios' root directory ($HELIOS_PATH)"
    exit 1
fi

file_missing=false

# Make sure Database configuration file (.pgpass) exists
if [ ! -f "$db_pgpass" ]; then
    echo "can't find database configuration file '$db_pgpass'"
    file_missing=true
fi

# Make sure Helios' settings.py is reachable
if [ ! -f "$helios_settings" ]; then
    echo "can't find Helios' '$helios_settings'"
    file_missing=true
fi

# Make sure Helios' 'reset.sh' is reachable as well
if [ ! -f "$helios_reset" ]; then
    echo "can't find Helios' '$helios_reset'"
    file_missing=true
fi

# Abort if any needed file is missing
if "$file_missing"; then
    exit 1
fi


# All set; configure files according to variable settings

# .pgpass
echo -n "Configuring .pgpass..."
configure_raw '$DB_HOST' "$db_pgpass"
configure_raw '$DB_PORT' "$db_pgpass"
configure_raw '$DB_USER' "$db_pgpass"
configure_raw '$DB_PASSWORD' "$db_pgpass"
echo "done!"

# settings.py
echo -n "Configuring settings.py..."
configure_quotes '$DEBUG' "$helios_settings"
configure_quotes '$HELIOS_PORT' "$helios_settings"
configure_quotes '$DB_NAME' "$helios_settings"
configure_quotes '$DB_USER' "$helios_settings"
configure_quotes '$DB_HOST' "$helios_settings"
configure_quotes '$DB_PASSWORD' "$helios_settings"
configure_quotes '$TIME_ZONE' "$helios_settings"
configure_quotes '$LANGUAGE_CODE' "$helios_settings"
configure_quotes '$DEFAULT_FROM_EMAIL' "$helios_settings"
configure_quotes '$DEFAULT_FROM_NAME' "$helios_settings"
configure_quotes '$HELP_EMAIL_ADDRESS' "$helios_settings"
configure_quotes '$EMAIL_HOST' "$helios_settings"
configure_quotes '$EMAIL_PORT' "$helios_settings"
configure_quotes '$EMAIL_HOST_USER' "$helios_settings"
configure_quotes '$EMAIL_HOST_PASSWORD' "$helios_settings"
configure_quotes '$EMAIL_USE_TLS' "$helios_settings"
configure_quotes '$BROKER_HOST' "$helios_settings"
echo "done!"

# reset.sh
echo -n "Configuring reset.sh..."
configure_quotes '$DB_HOST' "$helios_reset"
configure_quotes '$DB_USER' "$helios_reset"
configure_quotes '$DB_NAME' "$helios_reset"
configure_quotes '$ADMIN_LOGIN' "$helios_reset"
configure_quotes '$ADMIN_NAME' "$helios_reset"
configure_quotes '$ADMIN_PASSWORD' "$helios_reset"
echo "done!"
