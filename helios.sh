#!/bin/sh

####
## FOR USE BY DOCKER
####

# Fetch configuration parameters
. /home/helios/.config

# Wait until database is ready to respond
wait_database(){
    until psql -U $1 -h $2 -w -lqt > /dev/null; do
	echo "Database is not available yet -- please wait..."
	sleep 3
    done
}

# Detect if a database exists
database_exists(){
    psql -U $1 -h $2 -d $3 -w -lqt |
	tr -d [:blank:] |
	cut -d '|' -f1 |
	grep -qw $DB_NAME
}

# Create an empty database
create_database(){
    echo "Creating database $1..."
    /home/helios/helios-server/reset.sh
}

# Run the Helios Server along with the Celery daemon
run_helios(){
    python /home/helios/helios-server/manage.py runserver 0.0.0.0:$HELIOS_PORT &
    celery -A helios worker --loglevel=info
}

# Wait until DBMS is ready
wait_database $DB_USER $DB_HOST

# Force database creation if so requested
if [ "$1" = "-f" ]; then
    echo "Enforcing database creation"
    create_database $DB_NAME
else
    # Otherwise, check if a previous database exists
    if database_exists $DB_USER $DB_HOST $DB_NAME; then
	# Found a possible Helios database; attempt to run server
	echo "Database $DB_NAME already exists!"
	echo "Using existing database $DB_NAME..."
	run_helios
    else
	# No Helios databse found; attempt to create one
	echo "Could not access $DB_NAME on $DB_HOST with user $DB_USER!"
	echo "Assuming non-existent..."
	create_database $DB_NAME

	# Final check just to make sure the database was created successfully
	if database_exists $DB_USER $DB_HOST $DB_NAME; then
	    echo "Database $DB_NAME sucessfully created!"
	    run_helios
	else
	    echo "Error: could not create database $DB_NAME"
	fi
    fi
fi
