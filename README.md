# Helios on Docker

This repository provides a simple image recipe (i.e., a _Dockerfile_ and
associated resources) for building Docker containers with helios-server from
Helios Voting (https://heliosvoting.org/). The resulting image is ready to
accomodate translations to languages other than English (desired translations
should be placed inside the _locale_ directory prior to building the image).
A functional Brazilian Portuguese translation is already included. See the
[About Translations](https://gitlab.com/gggaspar/helios-docker#about-translations) section for further information.

The provided image is built and configured by default for testing purposes
only. This means some configuration parameters might be fairly insecure for
production standards, especially names and passwords, so adjust accordingly.


## TL;DR

   How to use helios-docker:

   1. Clone and enter the helios-docker repository:
      ```shell
      git clone https://gitlab.com/gggaspar/helios-docker/
      cd helios-docker
      ```
      
   2. Configure helios-docker by editing the _.config_ file
   
   3. Place any compatible translations inside the _locale_ directory
   
   4. Build and run the Docker stack for helios-docker:
      ```shell
      ./helios-docker.sh
      ```

## Access Credentials

   Upon successfully building and running the helios-docker container with
   default configurations, one should be left with a functional helios-server
   installation available at http://localhost:8000/ and an administrative
   account accessible with the following credentials:

   ```console
   username: admin
   password: password
   ```

   These credentials may be changed by modifying the corresponding variables
   in the _.config_ file found at the root level of helios-docker (this
   directory).
   

## Installation: the basic steps

### **1. Install Docker and Docker's Compose**

   One should make sure to have `docker` and `docker-compose` correctly
   installed in one's machine. Docker's Compose tool greatly simplifies
   the process of building and running Docker images. Installation of
   `docker` and `docker-compose` is highly dependent on OS choice. For
   more instructions and pointers, one should check
   [docker](https://www.docker.com/get-started) and
   [docker-compose](https://docs.docker.com/compose/install/).

   Do notice that it is perfectly possible to build and run helios-docker
   without aid from `docker-compose`: one will, however, need to manually
   load and configure the necessary images individually. Doing so is currently
   out of scope for this document, but ample documentation on doing so is
   freely available online. 


### **2. Download helios-docker**

   Download Helios Docker and enter its root directory:
   
   ```shell
   # git clone https://gitlab.com/gggaspar/helios-docker/  
   # cd helios-docker
   ```


### **3. Configure Helios installation**

   Helios installation can (hopefully) be fully configured by editing the
   _.config_ file at the root directory of helios-docker. This file contains
   various variables and respective values, each of which control specific
   aspects of building helios-docker and running Helios itself. Examples
   include the ports over which specific services will run, administrative
   credentials to Helios access and email server configuration.

   **Why a _.config_ file:** due to the intricate nature of Helios' code and
   many of its design decisions, there exist variables in certain files that,
   when changed, require compatible changes to variables in other files
   (e.g. changing database names require compatible changes to Helios'
   _settings.py_ and _reset.py_). Manually keeping track of such dependencies
   is highly prone to forgetfulness and human error, and failing to satisfy
   them may result in a fully broken system (or worse, a subtly broken system).
   This is why a central configuration file is provided: one only needs to care
   for a single central place to configure all necessary parameters and the
   provided configuration scripts make sure that those are enforced where
   necessary.

   For simple testing purposes, no configuration changes are required: the
   default ones are sane enough for a minimal local testing environment.

   The one change that could indeed be required for full functionality testing
   is setting Helios' email parameters, used by the server to send email to
   admins and voters. Such parameters, as expected, are found in the _.config_
   file, under the email server configuration section. By default, that section
   reads as follows:

   ```shell
   # Email server configuration
   EMAIL_HOST='localhost'
   EMAIL_PORT=2525
   EMAIL_HOST_USER=''
   EMAIL_HOST_PASSWORD=''
   EMAIL_USE_TLS=1
   ```

   One should change the above variables to the correct values according to the
   email server one wills to use. Such values are highly dependent on the
   specific email server to be used. As an example, a setting for a Gmail
   account would look like the following:

   ```shell
   # Email server configuration
   EMAIL_HOST='smtp.gmail.com'
   EMAIL_PORT=587
   EMAIL_HOST_USER='my_email_address@gmail.com'
   EMAIL_HOST_PASSWORD='my_email_password'
   EMAIL_USE_TLS=1
   ```
   
   Of course, do make sure to change at least _my\_email\_address@gmail.com_
   and _my\_email\_password_ with one's own values for email and corresponding
   password. Different email servers will probably use different values as
   well.

   By not defining the above email settings the Helios Server still works;
   only its email functionality will be disabled (some error messages
   regarding mail failures might show up as well if one is monitoring Helios'
   log).


## Using Docker's Compose

### Building the Docker stack

   **IMPORTANT:** for security and control purposes, the set of files that
   are imported inside Docker's build environment is fully specified via
   the _.dockerignore_ file. This means that Docker is enforced NOT to
   import anything other than what is specified in there. That allows for
   full control over what goes into the built container, filtering out
   possibly unwanted files that might have spilled inside the helios-docker
   root for one reason or another. Therefore, any new files or renamed files
   that are to be part of the helios-docker installation require a
   corresponding specification in _.dockerignore_. Docker will not see those
   files otherwise.
   
   Although a _docker-compose.yml_ file is plainly visible at the helios-docker
   root directory, one should not try to build the image via `docker-compose`
   up front. Such an attempt will probably end in warnings and errors regarding
   the validity of the compose file. This happens because, in order to provide
   a greater degree of configurability, the _docker-compose.yml_ file uses
   variables for many of its parameters. These variables need to be defined in
   a file by the name of _.env_ before the _docker-compose.yml_ file is
   executed. Those variables are already defined inside the _.config_ file, but
   Docker insists that its variables must be in a _.env_ file, which is also a
   little more strict in terms of syntax.

   Therefore, in order to build and run a Docker stack for helios-docker in a
   way that populates a _.env_ file that will make Docker happy, a main
   execution script is provided, `helios-docker.sh`. This script is responsible
   for building the _.env_ file taking into account the user configuration
   parameters as provided in _.config_. Thus, instead of trying to execute
   `docker-compose up` directly, one may set up the required docker stack
   fairly easily simply by issuing the following command (from inside the
   helios-docker root directory):

   ```console
   # ./helios-docker.sh
   ```

   The above command will take care of configuring the Docker build, then
   building the Docker stack necessary for Helios, configuring Helios itself
   and finally run the resulting Docker stack, all respecting the configuration
   parameters as set by the user in _.config_.

   **IMPORTANT:** the first time this command is run, all necessary (and not
   locally available) Docker images are downloaded from a configured Docker
   repository, so internet connection should probably be necessary (unless a
   local Docker repository is available and configured).

   The initial build can take quite a few minutes, so the opportunity could be
   used to exercise patience. By the end, one should have Helios up, running,
   and available at http://localhost:8000/ (considering the default
   configuration was used; again, adjust accordingly).


### Bringing the stack down

   Assuming the container is run in the foreground, the server can be brought
   down with a simple CTRL+C signal in the original terminal from which the
   application was started. The server may be brought back up with a simple
   execution of `helios-docker.sh`.
   

## What is the _helios.patch_ file for?

   The _helios.patch_ file, honoring its namesake, is a patch file for the
   Helios source code. Its main purpose is to **tag** every single English
   string that happens to exist throughout the original Helios tree so that
   the Django translation tools may be able to pick them up automatically
   from code and reunite them in a single place (a _django.po_ file). This
   allows for a centralized and easy translation effort. The _helios.patch_
   file, for such, was originally called _translation.patch_.

   However, the _helios.patch_ file now goes beyond simple translation and
   currently provides for two additional (and optional) functionalities:

   1. the possibility to limit voters to a single vote for a specific election
   2. the possibility to define voters' passwords via the very CSV file that
   is used to list voters (as a fourth field).

   The reason for the possibility to limit each voter to a single vote cast is
   due to a common misconception that people who are not familiar with the
   system have that the possibility to vote "as many times as one wants"
   implies in a single person being able to cast multiple valid votes that are
   included in the final tally. Although such a misconception can be remedied
   via careful and patient explanation, it surely raises more eyebrows than
   necessary. Such an option can be configured via an election's configuration
   page and defaults to the Helios standard of allowing for multiple votes by
   a single voter. If coercion is expected, an election admin should not toggle
   this option on (but then again, one should really question why is Helios
   being chosen for use under such a scenario in the first place).

   As for the second modification, it aims to allow for simple, presential
   ad-hoc elections in which voters gather collectively in a room to decide
   about something. If voters' passwords can be set via the CSV configuration
   file, election admins may prepare slips of paper in advance, each containing
   a voter's credentials for an election (or elections). These are then
   distributed to each individual voter at the start of the presential
   gathering. That way, one avoids dependency on internet and email
   infrastructure, its many unpredictable instabilities and the usual problem
   of "I did not receive the email with my voter credentials". Again, this can
   be configured via an election's configuration page and defaults to the
   standard of letting Helios generate a random password for each voter. If
   toggled, each voter must be provided a password as the fourth field of its
   line in the CSV file loaded for the election. Failing to do so results in an
   error and the voters file is not loaded.


## About Translations

   The image building procedure takes care of generating implemented
   translations (currently only pt-BR), which must be properly set
   inside the _locale_ directory of the helios-docker root. After
   bringing the server up, translations should already be fully
   functional. It is important to notice that translations are sensitive
   to the language the browser uses to request web pages, so accessing
   the server via a browser that requests pages in English should diplay
   Helios in plain default English. In order to access the server in
   Brazilian Portuguese, for example, the browser used should be set to
   request pages in Brazilian Portuguese (pt-BR).

   More information about how translations work and how to provide new
   ones for Helios can be found
   [here](https://github.com/gggaspar/helios-translation).


## Troubleshooting

   Since this project uses Docker, it tends to abstract away any complexities
   and idiosyncrasies of the underlying systems that run it. Therefore, it is
   hardly the case that the project correctly runs in one system but fails to
   correctly run in another one. Should one face such a situation, the first
   thing to check is whether the configuration parameters in the _.config_
   file have been correctly and sanely set. Should everything look sane but
   the system still malfunctions in any way, then one has most probably found
   a bug or a missing feature in the project. Please do report any
   inconsistencies via the GitLab **Issues** feature for this repository.

   Nevertheless, some common observations regarding apparent problems may be
   found in what follows:

   **1. When starting helios-docker I sometimes see _'psql: error'_ messages
   stating _'Connection refused'_.**
   - Those may happen when the helios container tries to connect to the
   database container, but the database container is not yet ready to
   accept connections. Should it occur, the helios container will patiently
   wait for a moment and then retry connecting. This will continue until the
   database is ready to accept connections. If helios keeps up with this retry
   cycle indefinitely, then something went wrong with the database container.

   **2. When starting helios-docker for the first time I can see _'FATAL'_
   messages from the helios and database containers stating _'database "xyz"
   does not exist'_.**
   - Those messages are due to the helios container testing if a database
   by the name _"xyz"_ (the database name as configured in _.config_) already
   exists. When it does not, then both the helios and database containers will
   issue a _FATAL_ error regarding the missing database. It sounds harmful, but
   it is a standard procedure: in such a case, the helios container will
   simply ask for the creation of a new database by the name _xyz_ and proceed
   with execution normally.

   **3. When starting helios-docker I can see some _'[Errno 111] Connection
   refused.'_ messages regarding the broker.**
   - This is normal and expected behaviour. When the system is starting, the
   main helios container tries to connect to the broker container, which may
   not _yet_ be available for connection. Thus, the connection to the broker
   will fail. That is not an issue, however: helios will continually try to
   reconnect to the broker until it succeeds. It is important to notice that
   successive retries happen in a crescent delay of time, so depending on how
   slow the running machine is to start the broker, the helios server may take
   some time to successfully boot up.

   **4. I see some error messages from the broker regarding closing AMQP
   connections along with a _'missed heartbeats from client, timeout'_
   message.**
   - The broker keeps a connection to the helios server so that both may
   communicate when message passing is required. When helios does not
   request anything from the broker for a certain amount of time, the
   broker will shut down such a connection. That is not an issue, however:
   should helios require messages to be passed, the connection to the
   broker will be automatically set back up again.